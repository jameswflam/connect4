import { Request,Response } from 'express';
import {Server as SocketIO } from 'socket.io';

export class OnlineGameController {
    constructor(
        private io:SocketIO) {
    }

    socket = async(req:Request,res:Response)=> {
        this.io.emit('YA','YAYAYAYAYAYAWOW');
        res.json({success: true})
    }

    getLocalPlayerName = async (req:Request,res:Response)=>{
        const player1Name = req.session['player1Name'];
        const player2Name = req.session['player2Name'];
        res.status(200).json({player1Name: player1Name, player2Name:player2Name});
    }

    // addLocalPlayerName = async (req:Request,res:Response)=>{
    //     const {player1_name, player2_name} = req.body;
    //     req.session['player1Name'] = player1_name;
    //     req.session['player2Name'] = player2_name;
    //     res.status(200).json({success: true});
    // }
}