const socket = io.connect()
const getCurrentUserInfo = await fetch('/getCurrentUserInfo')
const res = await fetch('/getCurrentUserInfo');
const userInfo = await res.json();
const username = userInfo.username;
const userId = userInfo.userId;
const points = userInfo.points;
const createRoom = document.querySelector('#createRoom-form')
const roomList = document.querySelector('.room-list')

// const roomContainer = document.querySelector('.room-container')
socket.emit("join_server", username, points)
socket.on('rooms', async (rooms) => {
    roomList.innerHTML = ``
    for (let room of rooms) {
        roomList.innerHTML += `
        <li class="list-group-item room-container"  data-id="${room.roomId}">
            <div class="card waiting-room">
                <h5 class="card-header">
                    <div class="room">Host: ${room.hostname}</div>
                </h5>
                <div class="card-body">
                    <div>
                        <h4 class="player-container">
                            <div id="redPlayer">Red: ${room.Player1}</div>
                            <div id="bluePlayer">Blue: ${room.Player2}</div>
                        </h4>
                    </div>
                    <div>
                        <button type="submit" class="btn btn-lg btn-info roomBtn">Enter</button>
                    </div>
                </div>
            </div>
        </li>
        `
        if (room.Player1 && room.Player2 != "waiting") {
            document.querySelector('.roomBtn').innerHTML = "Observe"
        }
    }
    const roomContainers = document.querySelectorAll('.room-container');
    for (let roomContainer of roomContainers) {
        roomContainer.querySelector('.roomBtn').onclick = function (event) {
            const roomId = roomContainer.getAttribute('data-id');
            console.log(roomId);
            setTimeout(() => {
                window.location = `/room.html?id=${roomId}`;
            }, 500);
        }
    }
})

createRoom.onsubmit = async function (event) {
    event.preventDefault();
    const form = event.target;
    const pickedColor = form.colors.value
    console.log(pickedColor);
    socket.emit("create_room", username, pickedColor)
    socket.on('redirect', (roomId) => {
        setTimeout(() => {
            window.location = `/room.html?id=${roomId}`;
        }, 500);
    })
}