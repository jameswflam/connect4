import { Game } from "./modules/Game.mjs";

const player1Layer = document.querySelector("#player1");
const player2Layer = document.querySelector("#player2");
const player1NameDiv = document.querySelector("#player1-name");
const player2NameDiv = document.querySelector("#player2-name");
const player1Circle = document.querySelector("#player1-circle");
const player2Circle = document.querySelector("#player2-circle");
const player1WinIcon = document.querySelector("#player1-win");
const player2WinIcon = document.querySelector("#player2-win");
const btnNewGame = document.getElementById("btn-new-game");
const circleLayers = document.querySelectorAll(".circle");
const gameMessageLayer = document.getElementById("game-message");

//playerSetting
let player1Name = "Player";
let player2Name = "Computer";
let humanColor = 1; //1: red, 2:blue
let difficulty = 4; //4:easy 5:medium 6:hard 7:hell

//game
let game;
let animation; //message animation
let computerTimeDelay = 500;

window.onload = async () => {
    addGameMessageAnimation();
    await addPlayerSetting();
    await newGame();
    addEventListener();
}

async function newGame() {
    game = new Game();
    await initGui();
}

async function initGui() {
    initGuiBoard();
    setGameMessage("");
    setGameMessage(`${player1Name}'s turn`);
    player1Circle.classList.add("player-circle-active");
    player2Circle.classList.remove("player-circle-active");
    player1WinIcon.classList.remove("player-win-active");
    player2WinIcon.classList.remove("player-win-active");
    player1Layer.classList.add("player-animation");
    player2Layer.classList.remove("player-animation");
    player2Layer.style.opacity = "0.6";
}

function initGuiBoard() {
    for (let circleLayer of circleLayers) {
        circleLayer.style.background = "whitesmoke";
    }
    for (let i = 0; i < game.board.rows; ++i) {
        for (let j = 0; j < game.board.columns; ++j) {
            let cell = document.getElementById("cell" + i + j);
            cell.className = cell.className.replace(/player[12]/, "");
            cell.style.border = "3px solid plum";
            if(humanColor == 1){
                cell.disabled = false;
                cell.onclick = function () {
                    humanPlayerMovement(this);
                }
            }
            else{
                cell.disabled = true;
            }
        }
    }
    if(humanColor == 2){
        setTimeout(computerPlayerMovement, computerTimeDelay);
    }
}

function setGameMessage(msg) {
    gameMessageLayer.style.opacity = 0;
    gameMessageLayer.textContent = msg;
    animation.restart();
}

function computerPlayerMovement(){
    let columnNumber = game.getComputerMovement(difficulty);
    drawCheckerInBoard(columnNumber);
    console.log(game.board.status)
    if (game.board.freeRowNumberPerColumn[columnNumber] != -1) {
        let cellToWhitesmoke = document.querySelector(`#cell${game.board.freeRowNumberPerColumn[columnNumber]}${columnNumber}`);
        cellToWhitesmoke.style.background = "whitesmoke";
        cellToWhitesmoke.style.animation = "none";
    }
    
    if(game.board.status == 0){
        for(let column in game.board.freeRowNumberPerColumn){
            if(game.board.freeRowNumberPerColumn[column] != -1){
                for (let i = 0; i < 6; i++) {
                    const cellToEnabled = document.getElementById(`cell${i}${column}`);
                    cellToEnabled.disabled = false;
                    cellToEnabled.onclick = function () {
                        humanPlayerMovement(this);
                    }
                }
            }
        } 
    }       
}

function humanPlayerMovement(cell) {
    let cellNumber = cell.id.substring(4, 6);
    let columnNumber = cellNumber[1];

    drawCheckerInBoard(columnNumber);

    for (let i = 0; i < game.board.rows; ++i) {
        for (let j = 0; j < game.board.columns; ++j) {
            let cellToDisabled = document.getElementById("cell" + i + j);
            cellToDisabled.disabled = true;
        }
    }

    if (game.board.freeRowNumberPerColumn[columnNumber] != -1) {
        let cellToWhitesmoke = document.querySelector(`#${cell.id.substring(0, 4)}${game.board.freeRowNumberPerColumn[columnNumber]}${columnNumber}`);
        cellToWhitesmoke.style.background = "whitesmoke";
        cellToWhitesmoke.style.animation = "none";
    }    
    if(game.board.status == 0){
        setTimeout(computerPlayerMovement, computerTimeDelay);
    }
}

function drawCheckerInBoard(columnNumber) {
    if (game.board.status != 0 || game.board.allCellsInColumnFilledIn(columnNumber)) {
        return;
    }
    let rowNumber = game.board.freeRowNumberPerColumn[columnNumber];
    let cellToColor = document.getElementById("cell" + rowNumber + "" + columnNumber);
    
    //update game board
    game.playerMovement(columnNumber);

    //updateGUI
    if (game.board.round == 1) {
        setGameMessage(player1Name + "'s turn");
        cellToColor.style.background = "blue";
        player2Circle.classList.remove("player-circle-active");
        player1Circle.classList.add("player-circle-active");
        player1Layer.classList.add("player-animation");  
        player2Layer.classList.remove("player-animation");  
    }
    else {
        setGameMessage(player2Name + "'s turn");
        cellToColor.style.background = "red";
        player1Circle.classList.remove("player-circle-active");
        player2Circle.classList.add("player-circle-active");
        player2Layer.classList.add("player-animation");
        player1Layer.classList.remove("player-animation");
    }
    cellToColor.classList.remove("circle-animation");

    //gameover
    if (game.board.status != 0) {
        showResult();
        return;
    }

    //update current columnNumber animation
    if (game.board.allCellsInColumnFilledIn(columnNumber)) {
        for (let i = 0; i < game.board.rows; i++) {
            const cellToDisabled = document.getElementById(`cell${i}${columnNumber}`);
            cellToDisabled.disabled = true;
        }
    }
    else {
        let cellToOppsiteColor = document.getElementById(`cell${rowNumber - 1}${columnNumber}`);
        if (game.board.round == 1) {
            cellToOppsiteColor.style.background = "rgb(255, 170, 170)";
        }
        else {
            cellToOppsiteColor.style.background = "rgb(160, 160, 255)";
        }
        cellToOppsiteColor.classList.add("circle-animation");
    }
}

function showResult() {
    if (game.board.status == 1) {
        setGameMessage(player1Name + " win")
        showWinningCell();
        player1Layer.style.opacity = "1";
        player2Layer.style.opacity = "0.6";
        player1WinIcon.classList.add("player-win-active");
    }
    else if (game.board.status == 2) {
        setGameMessage(player2Name + " win")
        showWinningCell();
        player2Layer.style.opacity = "1";
        player1Layer.style.opacity = "0.6";
        player2WinIcon.classList.add("player-win-active");
    }
    else if (game.board.status == 3) {
        setGameMessage("Draw")
        player1Layer.style.opacity = "1";
        player2Layer.style.opacity = "1";
    }

    player1Layer.classList.remove("player-animation"); 
    player2Layer.classList.remove("player-animation"); 
    player1Circle.classList.remove("player-circle-active");
    player2Circle.classList.remove("player-circle-active");
    for (let row = 0; row < game.board.rows; row++) {
        for (let column = 0; column < game.board.columns; column++) {
            let cell = document.getElementById("cell" + row + column);
            cell.disabled = true;
        }
    }
}

function showWinningCell() {
    for (let winningCell of game.board.winningArray) {
        const cell = document.querySelector(`#cell${winningCell[0]}${winningCell[1]}`);
        cell.style.border = "5px solid black";
    }
}

async function addPlayerSetting() {
    let res = await fetch('/single_game');
    let result = await res.json();
    if (result.color) {
        humanColor = result.color;
    }
    if (result.difficulty){
        difficulty = result.difficulty;
    }

    if (humanColor == 1) {
        if (result.playerName) {
            player1Name = result.playerName;
        }
        player1NameDiv.innerHTML = player1Name;
        player2NameDiv.innerHTML = "Computer";
    }
    else {
        player1Name = "Computer"
        if (result.playerName) {
            player2Name = result.playerName;
        }
        console.log(player1Name)
        player1NameDiv.innerHTML = "Computer";
        player2NameDiv.innerHTML = player2Name;
    }
}

function addEventListener() {
    btnNewGame.onclick = () => {
        newGame();
    }
    for (let circleLayer of circleLayers) {
        circleLayer.onmouseenter = (event) => {
            let columnNumber = event.target.id.substring(5);
            if (game.board.allCellsInColumnFilledIn(columnNumber)) {
                return;
            }
            let cell = document.querySelector(`#${event.target.id.substring(0, 4)}${game.board.freeRowNumberPerColumn[columnNumber]}${columnNumber}`);
            if (game.board.round == 1) {
                cell.style.background = "rgb(255, 170, 170)";
            }
            else {
                cell.style.background = "rgb(160, 160, 255)";
            }
            cell.classList.add("circle-animation");
        }
        circleLayer.onmouseleave = (event) => {
            let columnNumber = event.target.id.substring(5);
            if (game.board.allCellsInColumnFilledIn(columnNumber)) {
                return;
            }
            let cell = document.querySelector(`#${event.target.id.substring(0, 4)}${game.board.freeRowNumberPerColumn[columnNumber]}${columnNumber}`);
            cell.style.background = "whitesmoke";
            cell.classList.remove("circle-animation");
        }
    }
}

function addGameMessageAnimation() {
    animation = anime({
        loop: false,
        autoplay: false,
        targets: '#game-message',
        opacity: [0, 1],
        easing: "easeInOutQuad",
        duration: 1500,
    });
}