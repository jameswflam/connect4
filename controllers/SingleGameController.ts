import { Request, Response } from 'express';
import fetch from 'cross-fetch';
export class SingleGameController {
    constructor() {
    }

    addSingleGame = async (req: Request, res: Response) => {
        const { player_name, color, difficulty } = req.body;
        if(!player_name){
            if(req.session['user']){
                req.session['playerName'] = req.session['user'].username;
            }
            else{
                req.session['playerName'] = "Player";
            }
        }
        else{
            req.session['playerName'] = player_name;
        }
        req.session['color'] = color;
        req.session['difficulty'] = difficulty;
        res.status(200).json({ success: true });
    }

    getSingleGame = async (req: Request, res: Response) => {
        const playerName = req.session['playerName'];
        const color = req.session['color'];
        const difficulty = req.session['difficulty'];
        res.status(200).json({ playerName: playerName, color: color, difficulty: difficulty });
    }

    computerPlayerMovement = async (req: Request, res: Response) => {
        let { color, board } = req.body;
        let computerIndex;
        if(color == 0){
            computerIndex = 2;
        }
        else {
            computerIndex = 1;
        }

        // prints the board, used for debugging
        let str = ""
        for (let i = 0; i < 6; ++i) {
            for (let j = 0; j < 7; ++j) {
                str += board[i][j] + " "
            }
            str += "\n";
        }
        console.log(str);

        const pyRes = await fetch('http://localhost:8000/computerPlayerMovement', {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                computerIndex: computerIndex,
                board: board
            })         
        });
        const result = await pyRes.json();
        //Computer movement
        const columnNumber = result.randomResponse;
        
        res.status(200).json(columnNumber);
    }
}