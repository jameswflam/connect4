import express from 'express';
import { userController, localGameController ,singleGameController ,} from './app';
//import { isLoggedIn, isLoggedInAPI } from './guard';
import { isLoggedInAPI } from './guard';

export const routes = express.Router();

//Check user login status
routes.get('/login', isLoggedInAPI);

routes.get('/logout',userController.logout);
routes.post('/login',userController.login);
routes.post('/signUp',userController.signUp);
routes.get('/getCurrentUserInfo',userController.getCurrentUserInfo)

routes.get('/single_game', singleGameController.getSingleGame);
routes.put('/single_game', singleGameController.addSingleGame);
routes.put('/computerPlayerMovement', singleGameController.computerPlayerMovement);

routes.get('/local_name', localGameController.getLocalPlayerName);
routes.put('/local_name', localGameController.addLocalPlayerName);

// routes.get('/socket',onlineGameController.socket)

// routes.post('/create_room',onlineGameController.createRoom)