import {Board} from "./Board.mjs";

// generate AIgame
let board1 = new Board();
board1.board = [[1,0,0,1,0,0,0],
                [1,0,0,0,0,0,0],
                [0,1,0,1,0,0,0],
                [1,0,1,0,1,0,0],
                [0,1,0,0,0,0,0],
                [1,0,0,0,0,0,0]];

let win = checkEnd();
console.log(win)
if(win){
    console.log(board1.winningArray)
}

function checkEnd(){
    for(let row=0;row<board1.rows;row++){
        for(let column=0;column<board1.columns;column++){
            board1.updateStatus(row, column);
            if(board1.status != 0){
                return true;
            }
        }
    }
    return false;
}
