export interface User{
    id?:number
    email:string
    password:string
    username:string
}

export interface Room{
    roomId:string,
    hostname:string,
    Player1:string | undefined,   //red
    Player2:string | undefined,    //blue
    Player1Id: string | undefined,
    Player2Id: string | undefined,
    // nextPlayer: string | undefined,
}