import {RewardA2} from "./RewardA2.mjs";

export class MinimaxA2B2 {
    round;

    constructor(round){
        this.round = round;
    }

    maximizePlay (board, depth, alpha, beta) {
        const reward = new RewardA2(this.round, board);
        const score = reward.getReward();
    
        // Break
        if (this.isFinished(board, depth, score)) return [null, score];
    
        // Column, Score
        let max = [null, -99999999];
    
        // For all possible moves
        for (let column = 0; column < board.columns; column++) {
            if(board.turn == 0 && column == 3){
                continue;
            }

            let new_board = board.copy(); // Create new board
    
            if (!new_board.allCellsInColumnFilledIn(column)) {
                new_board.update(column);

                let next_move = this.minimizePlay(new_board, depth - 1, alpha, beta); // Recursive calling
    
                // Evaluate new move
                if (max[0] == null || next_move[1] > max[1]) {
                    max[0] = column;
                    max[1] = next_move[1];
                    alpha = next_move[1];
                }
    
                if (alpha >= beta) return max;
            }
        }
        return max;
    }

    minimizePlay (board, depth, alpha, beta) {
        const reward = new RewardA2(this.round, board);
        const score = reward.getReward();
    
        if (this.isFinished(board, depth, score)) return [null, score];
    
        // Column, score
        let min = [null, 99999999];
    
        for (let column = 0; column < board.columns; column++) {
            let new_board = board.copy();
    
            if (!new_board.allCellsInColumnFilledIn(column)) {
                new_board.update(column);
    
                let next_move = this.maximizePlay(new_board, depth - 1, alpha, beta);
    
                if (min[0] == null || next_move[1] < min[1]) {
                    min[0] = column;
                    min[1] = next_move[1];
                    beta = next_move[1];
                }
    
                if (alpha >= beta) return min;
            }
        }
        return min;
    }

    isFinished(board, depth, score){
        if (depth == 0 || score == this.winScore || score == -this.winScore || board.isFull()) {
            return true;
        }
        return false;
    }
}