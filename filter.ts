

function filter(numbers:number[],predicate:(number:number,number2:number)=>boolean){
    const filtered = [];
    for(let num of numbers){
        if(predicate(num,num)){
            filtered.push(num)
        }
    }
    return filtered;
}

export default filter;