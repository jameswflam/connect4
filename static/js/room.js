let res = await fetch('/getCurrentUserInfo');
let userInfo = await res.json();
let socket = io.connect();
let username;
let userId;
let points;
const chatForm = document.getElementById('chat-form');
const chatMessages = document.querySelector('.chat-messages');
const hostName = document.getElementById('host-name');
const opponentName = document.getElementById('opponent-name');
const observersList = document.getElementById('observer-list');
const roomDiv = document.querySelector('.room-detail');
const btnExit = document.getElementById("btn-exit");
const botMsg = document.querySelector('.botMsg');
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const roomId = urlParams.get('id')
console.log(roomId);
username = userInfo.username;
userId = userInfo.userId;
points = userInfo.points;

btnExit.addEventListener('click', async () => {
    await socket.emit("exit_room", username);
    window.location = '/matching.html'
})
socket.on('host_left', () => {
    console.log("host left")
    botMsg.innerHTML = "Host has left, redirecting to matching room..."
    setTimeout(() => {
        window.location = '/matching.html'
    }, 5000);
})

// socket.on('update_roomData')
socket.emit("join_room", roomId, username)
socket.on('game_data', (roomData) => {
    roomDiv.innerHTML =
        `<div class="Player1">Red: ${roomData.Player1} </div>
          <div class="Player2">Blue: ${roomData.Player2}</div>
          <button type="button" class="btn btn-secondary btn-lg" id="startBtn">Start</button>`
    outputHostName(roomData.hostname);
    console.log(roomData.Player1)
    console.log(roomData.Player2)
    if (roomData.Player1 != roomData.hostname) {
        outputOpponentName(roomData.Player1)
    } else if (roomData.Player2 != roomData.hostname){
    outputOpponentName(roomData.Player2)
}

const startBtn = document.querySelector('#startBtn')
if (roomData.Player1 && roomData.Player2 != "waiting") {
    if (roomData.hostname == username) {
        startBtn.addEventListener('click', function () {
            socket.emit('startBtn')
        })
    }
    if (roomData.hostname != username) {
        startBtn.innerHTML = "Waiting host to start"
    }
} else {
    startBtn.innerHTML = "Waiting for component..."
}
})


socket.on('roomObservers', (observers) => {
    outputObservers(observers);
});


// // Join chatroom
// socket.emit('joinRoom', { username, room });

// Get room and users


//chat-room socket
// Message from server
socket.on('message', (message) => {
    console.log(message);
    outputMessage(message);

    // Scroll down
    chatMessages.scrollTop = chatMessages.scrollHeight;
});

// Message submit
chatForm.addEventListener('submit', (e) => {
    e.preventDefault();

    // Get message text
    let msg = e.target.elements.msg.value;

    msg = msg.trim();

    if (!msg) {
        return false;
    }

    // Emit message to server
    socket.emit('chatMessage', msg, username);

    // Clear input
    e.target.elements.msg.value = '';
    e.target.elements.msg.focus();
});

// Output message to DOM
function outputMessage(message) {
    const div = document.createElement('div');
    div.classList.add('message');
    const p = document.createElement('p');
    p.classList.add('meta');
    p.innerText = message.username;
    p.innerHTML += `<span>${message.time}</span>`;
    div.appendChild(p);
    const para = document.createElement('p');
    para.classList.add('text');
    para.innerText = message.text;
    div.appendChild(para);
    document.querySelector('.chat-messages').appendChild(div);
}

// Add room name to DOM
function outputOpponentName(opponent) {
    opponentName.innerText = opponent;
}

function outputHostName(host) {
    hostName.innerText = host;
}

// Add users to DOM
function outputObservers(observers) {
    observersList.innerHTML = '';
    observers.observers.forEach(observer => {
        const li = document.createElement('li');
        li.innerText = observer.username;
        observersList.appendChild(li);
    });
}

//end of chat-room socket
socket.on('game_start', function () {
    setTimeout(() => {
        window.location = `/onlinegame.html?id=${roomId}`
    }, 1000)
})
