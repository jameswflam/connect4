import { NextFunction, Request, Response } from 'express';

export function isLoggedIn(req: Request, res: Response, next: NextFunction) {
    if (req.session['user']) {
        next();
    } else {
        res.redirect('/');
    }
}

export function isLoggedInAPI(req: Request, res: Response) {
    if (req.session['user']) {
        res.status(201).json({msg:"isLogin"});
    } else {
        res.status(200).json({msg:"isNotLogin"});
    }
}