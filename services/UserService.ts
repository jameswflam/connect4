import { Knex } from "knex";
import { hashPassword } from "../hash";
import { User } from "../models";

export class UserService{
    constructor(private knex:Knex){}

    async getUsers(email:string):Promise<User[]> {
        return this.knex.select('*').from('users').where('email',email);
    }

    async signUp(email:string,password:string,username:string,points:number){
        return this.knex.insert({
            email:email,
            password: await hashPassword(password),
            username:username,
            points: points,
        }).into('users');
    }

    // Randy: perhaps comment out below code could be better.
    // async updateScore(username:string,points:number){
    //     await this.knex('users').update({points:points}).where({username:username});
    // }
}