import { Board } from './Board.mjs'
import { Minimax } from './Minimax.mjs';

export class Game {
    board;

    constructor(round = 1, rows = 6, columns = 7, winCondition = 4) {
        this.board = new Board(round, rows, columns, winCondition);
        //for debugging
        console.log('New game' + `\n`);
    }

    playerMovement(columnNumber) {
        this.board.update(columnNumber);

        //for debugging
        console.log(`Player movement: ${columnNumber}`);
        this.board.print();
    }

    getComputerMovement(depth = 4) {
        if (this.board.status != 0) {
            console.log('Nothing happen')
            return;
        }
        if (depth > 0) {
            // Record AI decision time taken
            const startTime = new Date().getTime();

            // Algorithm call
            let minimax = new Minimax(this.board.round);
            const ai_move = minimax.maximizePlay(this.board, depth);

            // Print AI decision time taken
            const timeTaken = new Date().getTime() - startTime;

            // Place ai decision
            return ai_move[0];
        }
        else {
            let randamNumber = Math.floor(Math.random() * 7);
            while (this.board.allCellsInColumnFilledIn(randamNumber)) {
                randamNumber = (randamNumber + 1) % 7;
            }
            return randamNumber;
        }
    }

    restartGame(round = this.board.round) {
        let new_board = new Board(round, this.board.rows, this.board.columns, this.board.winCondition);
        this.board = new_board;

        //for debugging
        console.log('restart game' + `\n`);
    }
}